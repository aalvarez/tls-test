FROM alpine:latest

VOLUME "/etc/grid-security"
EXPOSE 443

ADD "tls-test" "/"
ENTRYPOINT ["/tls-test"]

