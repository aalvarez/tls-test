package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"flag"
	"fmt"
	"github.com/Sirupsen/logrus"
	"gitlab.cern.ch/flutter/go-proxy"
	"math/big"
	"net/http"
	"os"
	"time"
)

var hostcert = flag.String("cert", "", "Host certificate")
var hostkey = flag.String("key", "", "Host private key")
var tlsPort = flag.Int("tlsPort", 443, "Bind to this port for https")
var port = flag.Int("port", 80, "Bind to this port for plain http")
var capath = flag.String("capath", "", "CA Path")

var certPool = &proxy.CertPool{}

func handler(w http.ResponseWriter, req *http.Request) {
	logrus.Info("Got request")

	w.Header().Set("Content-Type", "text/plain")
	fmt.Fprint(w, "Hello there\n")

	if req.TLS != nil && req.TLS.PeerCertificates != nil {
		x509 := proxy.X509Proxy{}
		if err := x509.InitFromCertificates(req.TLS.PeerCertificates); err != nil {
			logrus.Error(err)
		} else {
			fmt.Fprint(w, "You are ", x509.Subject, "\n")
			for _, voms := range x509.VomsAttributes {
				fmt.Fprint(w, "Role ", voms.Fqan, " (", voms.Vo, ")\n")
			}
			fmt.Fprint(w, "Delegation ID ", x509.DelegationID(), "\n")
			fmt.Fprint(w, "Verification: ", x509.Verify(proxy.VerifyOptions{Roots: certPool}), "\n")
		}
	}
	hostname, _ := os.Hostname()
	fmt.Fprint(w, "URL: https://", hostname, ":", *tlsPort)
}

func generateCertificate() []tls.Certificate {
	hostname, err := os.Hostname()
	if err != nil {
		logrus.Fatal(err)
	}

	priv, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		logrus.Fatal(err)
	}

	notBefore := time.Now()
	notAfter := notBefore.Add(24 * time.Hour)

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		logrus.Fatal(err)
	}

	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			CommonName: hostname,
		},
		DNSNames:              []string{hostname},
		NotBefore:             notBefore,
		NotAfter:              notAfter,
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, &priv.PublicKey, priv)
	if err != nil {
		logrus.Fatal(err)
	}

	return []tls.Certificate{{
		Certificate: [][]byte{derBytes},
		PrivateKey:  priv,
		Leaf:        nil,
	}}
}

func main() {
	flag.Parse()
	if *hostcert == "" {
		*hostcert = os.Getenv("X509_HOST_CERT")
		*hostkey = os.Getenv("X509_HOST_KEY")
	}
	if *capath == "" {
		*capath = os.Getenv("X509_CA_PATH")
	}

	http.HandleFunc("/", handler)
	logrus.Info("Starting https://localhost:", *tlsPort)
	logrus.Info("Starting http://localhost:", *port)
	logrus.SetLevel(logrus.DebugLevel)

	tlsServer := &http.Server{
		Addr:    fmt.Sprintf(":%d", *tlsPort),
		Handler: nil,
		TLSConfig: &tls.Config{
			ClientAuth: tls.RequireAnyClientCert,
		},
	}
	server := &http.Server{
		Addr:    fmt.Sprintf(":%d", *port),
		Handler: nil,
	}

	if *hostcert == "" {
		logrus.Warn("Using auto-generated host certificate")
		tlsServer.TLSConfig.Certificates = generateCertificate()
	} else {
		logrus.Info("Using certificate ", *hostcert)
	}

	if *capath != "" {
		var err error
		logrus.Info("Loading CA Path ", *capath)
		if certPool, err = proxy.LoadCAPath(*capath, false); err != nil {
			logrus.Error(err)
		}
	}

	go func() {
		err := tlsServer.ListenAndServeTLS(*hostcert, *hostkey)
		logrus.Fatal(err)
	}()
	err := server.ListenAndServe()
	logrus.Fatal(err)
}
